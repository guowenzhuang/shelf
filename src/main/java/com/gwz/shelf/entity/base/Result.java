package com.gwz.shelf.entity.base;

import com.gwz.shelf.consts.ResponseCode;
import lombok.Data;

/**
 * @author 时间
 * @date 2021/3/5 14:08
 */
@Data
public class Result<T> {
    private int code;
    private boolean success;
    private String message;
    private T data;


    public Result() {
    }

    public Result(boolean success) {
        this.success = success;
        this.code = success ? ResponseCode.success.code : ResponseCode.faill.code;
    }

    public Result(boolean success, String message) {
        this.success = success;
        this.code = success ? ResponseCode.success.code : ResponseCode.faill.code;
        this.message = message;
    }

    public Result(boolean success, String message, T data) {
        this.success = success;
        this.code = success ? ResponseCode.success.code : ResponseCode.faill.code;
        this.message = message;
        this.data = data;
    }


    public static Result success() {
        return new Result<String>(true);
    }

    public static Result success(String message) {
        return new Result<String>(true, message);
    }


    public static Result success(String message, Object data) {
        return new Result(true, message, data);
    }

    public static Result fail() {
        return new Result<String>(false);
    }

    public static Result fail(String message) {
        return new Result(false, message);
    }


    public static Result fail(String message, Object data) {
        return new Result(false, message, data);
    }

}
