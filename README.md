# springboot基础架子(不包含权限处理)


* jdk11
* mysql
* druid
* redis
* druid监控
* mybaisplus
* data-redis
* logback日志处理
* logback  requestId&requestUrl&requestMethod处理
* 异常封装
* 统一返回封装
* 删除数据自从存入垃圾表
* date,LocalDate等请求响应自动转换
* insert/update自动更改create/update


## 统一返回封装
### 错误返回直接抛异常
示例
```java
        import cn.hutool.core.lang.Assert;
        Assert.state(false,"失败");
        //返回{"code":500,"success":false,"message":"java.lang.IllegalStateException: 失败","data":null}
```
### 正常返回
```java
    // 直接传字符串会封装到message里
    @GetMapping
    public String hello() {
        return "hello";
    }
    //返回{"code":200,"message":"hello","success":true}

    // 传除了字符串和Result其他类会封装到data里
    @GetMapping
    public JSONObject hello() {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name","jikeytang");
        return jsonObject;
    }
    // 返回{"code":200,"data":{"name":"jikeytang"},"success":true}
```
### 自定义返回
```java
    // 自定义result返回
    @GetMapping
    public Result hello() {
        return new Result();
    }
    
    //如果不想返回Result又想自定义可以使用@IgnoreResultAdvice
    @GetMapping
    @IgnoreResultAdvice
    public Result hello() {
        return new Result();
    }
```



## 注解
 @IgnoreResultAdvice 忽略对返回值封装可放在方法或者类上


